#! /usr/bin/env python3

import numpy as np
import torch

from varinf.model import BetaBinomialModel
from varinf.guide import BetaBinomialGuide

np.random.seed(0)

# generate some random data
data = torch.tensor(
    np.random.choice(2, size=1000, p=[0.7, 0.3]).astype(np.float32)
)

model = BetaBinomialModel(10.0, 10.0)
guide = BetaBinomialGuide(model)
model.fit(data, guide, niters=1000, lr=0.0005)
