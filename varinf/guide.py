#! /usr/bin/env python3

import torch
import pyro

from pyro.nn import PyroSample, PyroParam
from pyro.contrib.easyguide import EasyGuide
from pyro.distributions import constraints
from pyro import distributions as dist


class BetaBinomialGuide(EasyGuide):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._alpha_q = PyroParam(
            torch.tensor(15.0), constraint=constraints.positive
        )
        self._beta_q = PyroParam(
            torch.tensor(15.0), constraint=constraints.positive
        )
        self._p = PyroSample(dist.Beta(self._alpha_q, self._beta_q))

    def guide(self, data):
        pyro.sample("_p", dist.Beta(self._alpha_q, self._beta_q))
