#! /usr/bin/env python3

import pyro


from pyro.nn import PyroSample, PyroModule
from pyro import optim
from pyro import distributions as dist
from pyro.infer import SVI, Trace_ELBO


class BetaBinomialModel(PyroModule):
    def __init__(self, _hyper_alpha, _hyper_beta, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._p = PyroSample(dist.Beta(_hyper_alpha, _hyper_beta))

    def forward(self, data):
        with pyro.plate("data", len(data)):
            pyro.sample(
                "obs", dist.Binomial(len(data), self._p), obs=data.sum()
            )
        return

    def fit(
        self, data, guide, opt=optim.Adam,
        loss=Trace_ELBO(), lr=1e-2, niters=100
    ):

        optimizer = opt({"lr": lr})
        svi = SVI(self, guide, optimizer, loss=loss)
        pyro.clear_param_store()

        for j in range(1, niters+1):
            loss = svi.step(data)
            if j % 100 == 0:
                print(f"[iter {j}] loss: {loss:.4f}")
